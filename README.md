# Example Shiny app on GitLab

This is an example Shiny application that has its source code hosted on GitLab. This repository is very similar to another application on GitHub at [github.com/rstudio/shiny_example](https://github.com/rstudio/shiny_example). 
